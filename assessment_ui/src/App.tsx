import { useRef, useState } from 'react'
import { InputTextarea } from 'primereact/inputtextarea';
import { Button } from 'primereact/button';
import { Accordion, AccordionTab } from 'primereact/accordion';
import { Toast } from 'primereact/toast';
import './App.scss'
import "primereact/resources/themes/lara-light-cyan/theme.css";
//import "primereact/resources/themes/bootstrap4-light-blue/theme.css";
import 'primeicons/primeicons.css';
import axios from 'axios';

function App() {
  const [prompt, setPrompt] = useState("");
  const [loading, setLoading] = useState(false);
  const [aiResponse, setAiResponse] = useState<any[]>([]);
  const [activeAccordianIndexs, setActiveAccordianIndexs] = useState<number>(0);
  const toast = useRef<any>(null);

  const getChatResponse = async () => {
    setLoading(true);
    try {
      const response = await axios.post("/api/v1/prompt", { prompt });
      const combineResponse = {
        promptQuery: prompt,
        message: response.data.message
      };
      setPrompt("");
      const activeindex = aiResponse.length;
      setTimeout(() => {
        setActiveAccordianIndexs(activeindex);
        if (activeindex > 0) {
          (document.querySelectorAll('.p-accordion-tab a')[activeindex] as any).click();
        }
      }, 500);
      setAiResponse(prevState => [...prevState, ...[combineResponse]]);
    } catch (error: any) {
      toast?.current?.show({ severity: 'error', summary: 'Error', detail: 'Message Content', life: 3000 });
    } finally {
      setLoading(false);
    }
  }

  const sendPrompt = (event: any) => {
    if (event.key === 'Enter') {
      getChatResponse();
    }
  }

  const clickSeach = () => {
    getChatResponse();
  }

  const setValue = ({ target: { value } }: any) => {
    setPrompt(value);
  }

  return (
    <>
      <div className='absolute bottom-0 mb-5' style={{ width: '100%',height:'100%' }}>
        <div className="flex flex-column">
          <div className="flex align-items-center justify-content-center">
            <Accordion activeIndex={activeAccordianIndexs} style={{ width: "53%" }}>
              {
                aiResponse.map((response: any, index: number) => {
                  const { promptQuery, message: { content } } = response;
                  return (
                    <AccordionTab header={promptQuery} key={index}>
                      <p className="m-0">
                        {content}
                      </p>
                    </AccordionTab>
                  )
                })
              }
            </Accordion>
          </div>
          <div className="flex align-items-center justify-content-center">
            <span className="p-input-icon-left">
              <i className="pi pi-search" />
              <InputTextarea autoResize value={prompt} onChange={setValue} onKeyUp={sendPrompt} rows={1} cols={100} placeholder='How is your day?' style={{ height: 46 }} readOnly={loading} />
            </span>
            <Button label="" icon="pi pi-search" loading={loading} onClick={clickSeach} className='ml-1' />
          </div>
        </div>
      </div>
      <Toast ref={toast} />
    </>
  )
}

export default App
