# Assesment



## Getting started

Clone the repo in your local PC drive and run `npm install` followed by `npm run dev` under both `assessment_ui` and `assessment_server` folders

## Adding Open Ai API key

Inside the `.env` file under `assessment_server` add your open ai API key

## Running the APP

 open `localhost:5173` in your browser and enter prompt to get the results
