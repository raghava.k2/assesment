const { OpenAI } = require("openai");

const openai = new OpenAI({
    apiKey: process.env['OPENAI_API_KEY']
});

async function getChat(prompt) {
    try {
        const {choices} = await openai.chat.completions.create({
            messages: [{ "role": "user", "content": prompt }],
            model: "gpt-3.5-turbo",
        });
         
        return {choices};
    } catch (error) {
        console.error(error);
        return {error};
    }
}

module.exports = { getChat }