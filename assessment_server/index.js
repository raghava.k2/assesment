const express = require("express");
const { getChat } = require("./openai");
const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get("/", function (req, res) {
    res.send("server is up and running");
});

app.post("/v1/prompt", async (req, res) => {
    const { choices, error } = await getChat(req.body.prompt);
    if (error) {
        const { status, error: errMsg } = error
        res.status(status).json(errMsg);
    } else {
        res.json(choices[0]);
    }
})

app.listen(port, function () {
    console.log(`Assessment server listening on port ${port}!`);
});